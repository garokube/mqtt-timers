var mqtt = require('mqtt');
const fs = require('fs');
const express = require('express');
const app = express();
const port = 3000;

var mqtt_broker = 'mqtt://172.16.140.5';
if (process.env['MQTT_BROKER']) {
  mqtt_broker = process.env['MQTT_BROKER'];
}

var timers = new Map();

const client = mqtt.connect(mqtt_broker, {
  clientId: 'mqtt-timers'
});


client.on('connect', function () {
  const topic = 'timers/#';
  client.subscribe(topic, { nl: true }, function (err) {
    if (!err) {
      console.log("Subscribed to", topic);
    }
  })
})
 
client.on('message', function (topic, message) {
  // message is a Buffer
  /*
                  "onActive": {
                    "topic": "zigbee2mqtt/usb_lataus",
                    "payload": {
                        "state": "ON"
                    }
                },
                "onDeactivation": {
                    "topic": "zigbee2mqtt/usb_lataus",
                    "payload": {
                        "state": "OFF"
                    } 
                }
  */
  console.log("topic:", topic, "message:", message.toString());
  
  let str = message.toString();
  if (str == "") {
    return;
  }

  try {
    timers[topic] = JSON.parse(str);
  } catch (e) {
    console.log("Unable to parse JSON from topic", topic, "json:", str);
  }

  refreshTimer(topic);
});

function refreshTimers() {
  for (let [name, value] of Object.entries(timers)) {
    refreshTimer(name);
  }
}

function refreshTimer(name) {
  const timer = timers[name];

  const t = timer['time'];
  const currentTime = Math.floor(Date.now()/1000);

  if (t > currentTime) {
    if (timer['onActive']) {
      const str = JSON.stringify(timer['onActive']['payload']);
      console.log("Sending onActive message for timer", name, "topic is", timer['onActive']['topic'], "and message:", str);
      client.publish(timer['onActive']['topic'], str, {retain: true});
    }

  } else {
    if (timer['onDeactivation']) {
      const str = JSON.stringify(timer['onDeactivation']['payload']);
      console.log("Sending onDeactivation message for timer", name, "topic is", timer['onDeactivation']['topic'], "and message:", str);
      client.publish(timer['onDeactivation']['topic'], str, {retain: true});
    }
  }

  const seconds = t - currentTime;
  if (seconds < -60) {
    client.publish(name, null, {retain: true});
  }

  scheduleNextTimer();
}

let timerHandler = null;
function scheduleNextTimer() {
  let nextTimer = Number.MAX_SAFE_INTEGER;
  let nextKey = "";
  for (let [key, value] of Object.entries(timers)) {
    if (value['time'] < nextTimer) {
      nextKey = key;
      nextTimer = value['time'];
    }
  }
  const ms = nextTimer * 1000 - Date.now() 
  
  if (timerHandler != null) {
    clearTimeout(timerHandler);
  }
  if (ms > 0) {
    console.log("Next timer", nextKey, "will trigger in", ms, "ms");
    timerHandler = setTimeout(refreshTimers, ms);
  }
}

app.listen(port, () => console.log(`mqtt-timers listening on port ${port}!`))



